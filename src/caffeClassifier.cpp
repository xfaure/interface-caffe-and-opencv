/***************************************************************************
*                                                                         *
*   Copyright (C) 2017 Xavier Faure                                       *
*                                                                         *
*   @Author: Xavier Faure												  *
*   @Contact: xavier.faure42@gmail.com    								  *
*                                                                         *
***************************************************************************/

#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"

#include "caffeClassifier.h"


using namespace std;
using namespace cv;


CaffeClassifier::CaffeClassifier()
    : mName("CaffeNet")
{
}

CaffeClassifier::CaffeClassifier(const string & name,
    const cv::Size &imageSize)
    : mName(name), mInputSize(imageSize)
{
}

CaffeClassifier::~CaffeClassifier()
{}

int CaffeClassifier::Predict(const Mat &query)
{

    Mat inputNormalized;
    normalizeImage(query, inputNormalized);

    dnn::Blob inputBlob = dnn::Blob::fromImages(inputNormalized);

	mNet.setBlob(".data", inputBlob);
	mNet.forward();

    dnn::Blob prob = mNet.getBlob("prob");
        
    int classId;
    double classProb;
    getMaxClass(prob, &classId, &classProb);//find the best class

    return classId;
}

void CaffeClassifier::Predict(const vector<Mat> &queries, vector<int> &results)
{
   
    vector<Mat> inputsNormalized;
    for (auto it : queries)
    {
        Mat inputNormalized;
        normalizeImage(it, inputNormalized);
        inputsNormalized.push_back(inputNormalized);		
    }
            
    dnn::Blob inputBlob = dnn::Blob::fromImages(inputsNormalized);

	mNet.setBlob(".data", inputBlob);
	mNet.forward();

    dnn::Blob prob = mNet.getBlob("prob");

    vector<double> probabilites;
    getMaxClass(prob, results, probabilites);
}

void CaffeClassifier::normalizeImage(const Mat &input, cv::Mat &inputNormalized)
{
    Mat inputChannel;
    if (input.channels() < 3)
        cvtColor(input, inputChannel, CV_GRAY2BGR);
    else
        inputChannel = input;

    Mat rgbResized;
    resize(inputChannel, rgbResized, mInputSize);

    cv::Mat rgb32F;
    rgbResized.convertTo(rgb32F, CV_32FC3);

    subtract(rgb32F, mMean, inputNormalized);
}

/* Find best class for the blob (i. e. class with maximal probability) */
void CaffeClassifier::getMaxClass(dnn::Blob &probBlob, int *classId, double *classProb)
{		
    Mat probMat = probBlob.matRefConst().reshape(1, 1); //reshape the blob to 1x2 matrix
    Point classNumber;
    minMaxLoc(probMat, NULL, classProb, NULL, &classNumber);
    *classId = classNumber.x;
}

void CaffeClassifier::getMaxClass(cv::dnn::Blob &probBlob, std::vector<int> &classIds, std::vector<double> &classProbs)
{
    Mat probMat = probBlob.matRefConst().reshape(1, 1); //reshape the blob to 1x2xBatchSize matrix
    int batchSize = probMat.cols / 2;
    int resultsIndex = 0;
    for (int i = 0; i <  probMat.cols; i+=2)
    {
        Mat subMat(probMat(Range(0, 1), Range(i, i + 2)));
        Point classNumber;
        double prob;
        minMaxLoc(subMat, NULL, &prob, NULL, &classNumber);
        classIds.push_back(classNumber.x+1);
        classProbs.push_back(prob);
    }
}


bool CaffeClassifier::Load(const string &dirPath)
{
  
	mNet = dnn::readNetFromCaffe(
		dirPath + "/caffenet_model" + ".prototxt",
		dirPath + "/caffenet_weights" + ".caffemodel");
   
    Mat mean;
    FileStorage fs(dirPath + "/mean" + ".yml", FileStorage::READ);
    fs["mean"] >> mMean;
    fs.release();

	if (mMean.empty())
	{
		cout << "//!!!\\ Mean file was empty. Using ZERO by default." << endl;
		mMean = Mat::zeros(mInputSize, CV_32F);
	}

    return !mNet.empty();
}

