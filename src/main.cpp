/***************************************************************************
*                                                                         *
*   Copyright (C) 2017 Xavier Faure                                       *
*                                                                         *
*   @Author: Xavier Faure												  *
*   @Contact: xavier.faure42@gmail.com    								  *
*                                                                         *
***************************************************************************/

#include "caffeClassifier.h"

#include "opencv2/core.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"

int main()
{
	//Configuration
	//Default input size the classifier was trained for.
	cv::Size inputSize(227, 227);
	std::string dataPath = "../../data";
	std::string imagePath = "../../images";


	//Create the classifier.
	CaffeClassifier caffeNet("CaffeNet", inputSize);

	//Load it.	
	if (!caffeNet.Load(dataPath))
	{
		std::cerr << "Error loading DNN" << std::endl;
		return -1;
	}

	//Use it.
	std::string letterIds[10]{ "A", "B", "C", "D", "E", "F", "G", "H", "I", "J" };	
	for (unsigned int i = 0; i < 10; ++i)
	{
		//Load test images and resize it to classifier input size.
		cv::Mat testImage = cv::imread(imagePath + "/" + letterIds[i] + ".png");
		cv::resize(testImage, testImage, inputSize);

		//Classifiy the image and show result.
		int classId = caffeNet.Predict(testImage);
		if (classId > 9 || classId < 0)
		{
			std::cerr << "Error in classification, ID is bigger than 10..." << std::endl;
			continue;
		}
		else
		{
			cv::putText(testImage, letterIds[classId], cv::Point(10, 30), CV_FONT_HERSHEY_PLAIN, 2, cv::Scalar(255, 0, 0), 2);
			cv::imshow("Query", testImage);
			cv::waitKey(500);
			std::cout << letterIds[i] <<  " classified as " << letterIds[classId] << std::endl;
		}
	}
	std::cout << "Press a key to exit." << std::endl;
	cv::waitKey(0);
	return 1;
}