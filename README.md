
# Using a trained Caffe model with OpenCV


This piece of code was developed in the spirit to serve as tutorial material, to get the basics of using a model previously trained with Caffe in a OpenCV/C++ environnement.

This repository contains all the code needed to use any of your pre trained Caffe model.

This sample uses a trained bvlc_alexnet finetuned with a small subset of the [notMNIST dataset](http://yaroslavvb.blogspot.fr/2011/09/notmnist-dataset.html).


## Requirements

- CMake
- OpenCV >= 3.0 compiled with opencv_dnn module


## Content

- `main.cpp`: Running the sample, classifying 10 images as [A, B,..., J]
- `caffeClassifier.h`: Declaration of the classifier using a Caffe model
- `caffeClassifier.cpp`: Definition of the classifier using a Caffe model
- `images` : 10 test images from the [notMNIST dataset](http://yaroslavvb.blogspot.fr/2011/09/notmnist-dataset.html)
- `data/caffe_weights.prototxt` : The trained and finetuned weights of bvlc_alexnet
- `data/mean.yml` : Dummy mean file which is supposed to be subtracted to each test image


## Usage

Download the .caffemodel file from [Bitbucket](https://bitbucket.org/xfaure/interface-caffe-and-opencv/downloads/caffenet_weights.caffemodel) and cut/paste it in the ./data folder.

Then, all you need to modify is the `main.cpp`, changing path for the `data` and `images` folder. 
I still recommend to take a look at the whole code, as it's fairly compact and commented.