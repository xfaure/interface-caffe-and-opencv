/***************************************************************************
*                                                                         *
*   Copyright (C) 2017 Xavier Faure                                       *
*                                                                         *
*   @Author: Xavier Faure												  *
*   @Contact: xavier.faure42@gmail.com    								  *
*                                                                         *
***************************************************************************/

#ifndef CAFFE_CLASSIFIER_H
#define CAFFE_CLASSIFIER_H

#include "opencv2/core.hpp"
#include "opencv2/dnn.hpp"

class CaffeClassifier 
{
public:
	/**
	* Default constructor.	
	*/
	CaffeClassifier();

	/**
	* Constructor.
	*
	* @param name The classifier name.
	* @param imageSize The input image size.	
	*/
	CaffeClassifier(const std::string & name,
		const cv::Size &imageSize);

	/**
	* Destructor.
	*/
	virtual ~CaffeClassifier();

	/**
	* Classifies the given image.
	*
	* @param query The image to classify.
	* @return The predicted label.
	*/
	int Predict(const cv::Mat & query);

	/**
	* Classifies a batch of images.
	*
	* @param queries The image batch to classify.
	* @param results (output)The predicted labels.
	*/
	void Predict(const std::vector<cv::Mat> &queries, std::vector<int> &results);

	/**
	* Loads the pre-trained weights of a classifier.
	*
	* @param dirPath Path to the directory containing the caffenet_model*.prototxt|caffemodel
	* @return True for success, false otherwise.
	*/
	bool Load(const std::string &dirPath);


protected:        

	/**
	* Mean image.
	* Will be substracted to all predicted images. See below for mor details.
	*/
    cv::Mat mMean;

	/**
	* Name of the classifier.
	*/
    std::string mName;

	/**
	* Size of the input image it was trained for.
	*/
    cv::Size mInputSize;

	/**
	* The network as defined per OpenCV	
	*/
	cv::dnn::Net mNet;

	/**
	* Normalize the image before classifying it. Mainly removes the mean.
	*
	* @param input The input image.
	* @param inputNormalized (output) The normalized image.
	*/
	void normalizeImage(const cv::Mat &input, cv::Mat &inputNormalized);

	/**
	* Gets the class ID with the highest probability from a onehot encoding.
	*
	* @param probBlob The probabilty blob which is the output of a softmax.
	* @param classId (output) The class ID with highest probability
	* @param classProb (output) The highest probability.
	*/
	void getMaxClass(cv::dnn::Blob &probBlob, int *classId, double *classProb);

	/**
	* Gets the class ID with the highest probability from a onehot encoding for a batch of images.
	*
	* @param probBlob The probabilty blob which is the output of a softmax.
	* @param classId (output) The class IDs with highest probabilities for each batch.
	* @param classProb (output) The highest probabilities for each batch.
	*/
	void getMaxClass(cv::dnn::Blob &probBlob, std::vector<int> &classIds, std::vector<double> &classProbs);
};
#endif

//*****************NOTE ON mMEAN *******************//
////Mean image of all the training set. Must be pre-computed once and for all.
////Here is a sample code to do so	
//cv::Mat mean = cv::Mat::zeros(227, 227, CV_32FC3);
////Assuming trainingSet is a set of real images...
//std::vector<cv::Mat> trainingSet { cv::Mat::zeros(227, 227, CV_32FC3), cv::Mat(227, 227, CV_32FC3, cv::Scalar(1,1,1))};
//for(size_t i = 0; i < trainingSet.size(); ++i)
//{
//    cv::Mat train_32F;
//    trainingSet[i].convertTo(train_32F, CV_32FC3);
//    mean += train_32F;    
//}
//mean /= cv::saturate_cast<float>(trainingSet.size());
//cv::FileStorage outMean("mean.yml", cv::FileStorage::WRITE);
//outMean << "mean" << mean;
//outMean.release();
//************************************//